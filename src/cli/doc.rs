pub mod math {
    pub const FUNCTIONS: &str = r#"* print(...strings and values...) -- Prints to stderr.  Very useful to 'probe' an expression.
                                        Evaluates to the last value.
                                        Example: `print("x is", x, "and y is", y)`
                                        Example: `x + print("y:", y) + z == x+y+z`

* log(base=10, val) -- Logarithm with optional 'base' as first argument.
                        If not provided, 'base' defaults to '10'.
                        Example: `log(100) + log(e(), 100)`

* e()  -- Euler's number (2.718281828459045)
* pi() -- π (3.141592653589793)

* int(val)
* ceil(val)
* floor(val)
* round(modulus=1, val) -- Round with optional 'modulus' as first argument.
                            Example: `round(1.23456) == 1  &&  round(0.001, 1.23456) == 1.235`

* abs(val)
* sign(val)

* min(val, ...) -- Example: `min(1, -2, 3, -4) == -4`
* max(val, ...) -- Example: `max(1, -2, 3, -4) == 3`

* sin(radians)    * asin(val)
* cos(radians)    * acos(val)
* tan(radians)    * atan(val)
* sinh(val)       * asinh(val)
* cosh(val)       * acosh(val)
* tanh(val)       * atanh(val)"#;
    pub const OPERATORS: &str = r#"Listed in order of precedence:

(Highest Precedence) ^               Exponentiation
                        %               Modulo
                        /               Division
                        *               Multiplication
                        -               Subtraction
                        +               Addition
                        == != < <= >= > Comparisons (all have equal precedence)
                        && and          Logical AND with short-circuit
(Lowest Precedence)  || or           Logical OR with short-circuit"#;
    pub const LITERALS: &str = r#"Several numeric formats are supported:

Integers: 1, 2, 10, 100, 1001

Decimals: 1.0, 1.23456, 0.000001

Exponents: 1e3, 1E3, 1e-3, 1E-3, 1.2345e100

Suffix:
        1.23p        = 0.00000000000123
        1.23n        = 0.00000000123
        1.23µ, 1.23u = 0.00000123
        1.23m        = 0.00123
        1.23K, 1.23k = 1230
        1.23M        = 1230000
        1.23G        = 1230000000
        1.23T        = 1230000000000"#;
}

pub mod glyphs {
    pub const ALL: &str = r#"rest: .
pizz.: '°
volume: +-
length: /\
octave: ><
comment?: ;,
start here: ':'
slope: {{MODIFIER EXPR score}}
note modifier prefix: n
volume modifier prefix: v
octave modifier prefix: o
length modifier prefix: l
tempo modifier prefix: t
loop: ()
loop with count: (COUNT score)
tuple: []
modifier: !
volume modifier prefix: v
octave modifier prefix: o
length modifier prefix: l
tempo modifier prefix: t

EXAMPLE
aabc.
'ab°A
+d+d+d---
/ff/f\\
ab>c<ba
;this is a comment (or lyrics whatever),
C:CC
(3deff)
(deff)
[ffe]
{{l 1-cos((PI*x)/2),acced}}
abbc!o5cc!v15feed!l4fedd!t60Gdd"#;
}
