use std::{fs::File, io, str::FromStr};

use amplify::{From, Wrapper};
use fasteval::Compiler;
use thiserror::Error;
use tune::scala::{Kbm, KbmImportError, Scl, SclImportError};

use crate::bng::{KeyboardMapping, Scale};

use super::super::Expression;

impl FromStr for Expression {
    type Err = fasteval::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parser = fasteval::Parser::new();
        let mut slab = fasteval::Slab::new();
        Ok(parser
            .parse(s, &mut slab.ps)?
            .from(&slab.ps)
            .compile(&slab.ps, &mut slab.cs)
            .into())
    }
}

#[derive(Debug, Error)]
#[error("{0:?}")]
pub struct ScalaImportError<E>(E);

impl FromStr for Scale {
    type Err = ScalaImportError<SclImportError>;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Scl::import(
            File::open(s)
                .map_err(Into::into)
                .map_err(ScalaImportError)?,
        )
        .map(Scale::from)
        .map_err(ScalaImportError)
    }
}

impl FromStr for KeyboardMapping {
    type Err = ScalaImportError<KbmImportError>;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Kbm::import(
            File::open(s)
                .map_err(Into::into)
                .map_err(ScalaImportError)?,
        )
        .map(KeyboardMapping::from)
        .map_err(ScalaImportError)
    }
}
