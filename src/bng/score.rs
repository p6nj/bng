use std::{
    error::Error,
    num::{NonZeroU16, NonZeroU8},
};

use amplify::From;
use anyhow::Context;
use bng_macros::{ModifierParser, QuickModifierParser, SlopeModifierParser};
use derive_new::new;
use derived_deref::Deref;
use nom::{
    branch::alt,
    character::{complete::char, streaming::one_of},
    combinator::{all_consuming, eof},
    multi::many0,
    sequence::{preceded, terminated},
};
use serde::{
    de::{self as serde_de, Visitor},
    Deserialize,
};
use strum::EnumDiscriminants;
use thiserror::Error;

mod de;
mod lex;
pub use de::*;

use super::Expression as Instruction;

#[derive(Deref, From, Default, Clone)]
#[cfg_attr(debug_assertions, derive(Debug, PartialEq))]
pub struct Atoms(Vec<Atom>);

#[cfg_attr(debug_assertions, derive(Debug, PartialEq))]
pub enum Atom {
    Note(u8),
    Rest,
    StartHere,
    Modifier(Modifier),
    QuickModifier(QuickModifier),
    Loop(NonZeroU8, Atoms),
    Tuple(Atoms),
    Slope(SlopeModifier, Instruction, Atoms),
    Comment,
}

impl Clone for Atom {
    fn clone(&self) -> Self {
        match self {
            Self::Note(n) => Self::Note(*n),
            Self::Rest => Self::Rest,
            Self::StartHere => Self::StartHere,
            Self::Modifier(m) => Self::Modifier(m.clone()),
            Self::QuickModifier(q) => Self::QuickModifier(q.clone()),
            Self::Comment => Self::Comment,
            _ => unimplemented!("variant can't be cloned right now"),
        }
    }
}

#[derive(Clone, ModifierParser)]
#[cfg_attr(debug_assertions, derive(Debug, PartialEq))]
pub enum Modifier {
    Volume(u8),
    Octave(u8),
    Length(NonZeroU8),
    Tempo(NonZeroU16),
}

impl Default for Modifier {
    fn default() -> Self {
        Modifier::Volume(Default::default())
    }
}

#[derive(QuickModifierParser, Clone)]
#[cfg_attr(debug_assertions, derive(Debug, PartialEq))]
pub enum QuickModifier {
    Volume(bool),
    Octave(bool),
    Length(bool),
    Pizz(bool),
}

#[derive(Clone, Copy, SlopeModifierParser, Debug)]
#[cfg_attr(debug_assertions, derive(PartialEq))]
pub enum SlopeModifier {
    Note,
    Volume,
    Octave,
    Length,
    Tempo,
}
