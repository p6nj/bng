use const_format::concatcp;
use nom::{
    error::{Error, ErrorKind, VerboseError},
    Err,
};

use flat_atom::{
    FASTEVAL_INSTRUCTION as FLATATOM_FASTEVAL_INSTRUCTION, SAMPLE_STR as FLATATOM_SAMPLE_STRING,
};

fn set_notes() {
    super::set_notes("abcdefg").unwrap();
}

type NomError<'a> = VerboseError<&'a str>;

mod flat_atom {
    use std::num::NonZeroU8;

    use fasteval::Compiler;
    use lazy_static::lazy_static;
    use nom::Parser;

    use crate::bng::score::Atoms;

    use super::super::{
        super::super::super::Expression as Instruction, super::UP, atom, Atom, Modifier,
        QuickModifier, SlopeModifier,
    };
    use super::*;

    pub(super) const SAMPLE_STR: &str = concatcp!(
        Atom::TUPLE.0,
        "acc",
        Atom::TUPLE.1,
        "ed",
        Atom::COMMENT.0,
        "hello",
        Atom::COMMENT.1
    );

    pub(super) fn sample_str_expr() -> Atoms {
        Atoms(vec![
            Atom::Tuple(Atoms(vec![Atom::Note(0), Atom::Note(2), Atom::Note(2)])),
            Atom::Note(4),
            Atom::Note(3),
            Atom::Comment,
        ])
    }

    pub(super) const FASTEVAL_INSTRUCTION: &str = "1-cos((PI*x)/2)";

    #[test]
    fn note() {
        set_notes();
        assert_eq!(
            Ok((SAMPLE_STR, Atom::Note(2))),
            atom::<NomError>.parse(concatcp!('c', SAMPLE_STR))
        )
    }

    #[test]
    fn rest() {
        assert_eq!(
            Ok((SAMPLE_STR, Atom::Rest)),
            atom::<NomError>.parse(concatcp!(Atom::REST, SAMPLE_STR))
        )
    }

    #[test]
    fn start_here() {
        assert_eq!(
            Ok((SAMPLE_STR, Atom::StartHere)),
            atom::<NomError>.parse(concatcp!(Atom::START_HERE, SAMPLE_STR))
        )
    }

    #[test]
    fn modifier() {
        assert_eq!(
            Ok((
                SAMPLE_STR,
                Atom::Modifier(Modifier::Length(unsafe { NonZeroU8::new_unchecked(2) }))
            )),
            atom::<NomError>.parse(concatcp!(Atom::MODIFIER, Modifier::LENGTH, 2u8, SAMPLE_STR))
        )
    }

    #[test]
    fn quick_modifier() {
        assert_eq!(
            Ok((SAMPLE_STR, Atom::QuickModifier(QuickModifier::Length(UP)))),
            atom::<NomError>.parse(concatcp!(QuickModifier::LENGTH.0, SAMPLE_STR))
        )
    }

    #[test]
    fn r#loop() {
        set_notes();
        assert_eq!(
            Ok((
                SAMPLE_STR,
                Atom::Loop(unsafe { NonZeroU8::new_unchecked(3) }, sample_str_expr())
            )),
            atom::<NomError>.parse(concatcp!(
                Atom::LOOP.0,
                3u8,
                SAMPLE_STR,
                Atom::LOOP.1,
                SAMPLE_STR
            ))
        );
        assert_eq!(
            Ok((
                SAMPLE_STR,
                Atom::Loop(unsafe { NonZeroU8::new_unchecked(2) }, sample_str_expr())
            )),
            atom::<NomError>.parse(concatcp!(
                Atom::LOOP.0,
                SAMPLE_STR,
                Atom::LOOP.1,
                SAMPLE_STR
            ))
        );
        assert_eq!(
            Err(nom::Err::Failure(Error::new(
                concatcp!(0u8, SAMPLE_STR),
                ErrorKind::MapOpt
            ))),
            atom.parse(concatcp!(Atom::LOOP.0, 0u8, SAMPLE_STR))
        )
    }

    #[test]
    fn tuple() {
        set_notes();
        assert_eq!(
            Ok((SAMPLE_STR, Atom::Tuple(sample_str_expr()))),
            atom::<NomError>.parse(concatcp!(
                Atom::TUPLE.0,
                SAMPLE_STR,
                Atom::TUPLE.1,
                SAMPLE_STR
            ))
        )
    }

    #[test]
    fn slope() {
        set_notes();
        assert_eq!(
            Ok((
                SAMPLE_STR,
                Atom::Slope(
                    SlopeModifier::Note,
                    FASTEVAL_INSTRUCTION.parse().unwrap(),
                    sample_str_expr()
                )
            )),
            atom::<NomError>.parse(concatcp!(
                Atom::SLOPE.0,
                SlopeModifier::NOTE,
                ' ',
                FASTEVAL_INSTRUCTION,
                ',',
                SAMPLE_STR,
                Atom::SLOPE.1,
                SAMPLE_STR
            ))
        )
    }

    #[test]
    fn comment() {
        set_notes();
        assert_eq!(
            Ok((SAMPLE_STR, Atom::Comment)),
            atom::<NomError>.parse(concatcp!(
                Atom::COMMENT.0,
                "hi I'm a little pony",
                SAMPLE_STR,
                Atom::COMMENT.1,
                SAMPLE_STR
            ))
        )
    }

    #[test]
    fn nested_comments() {
        set_notes();
        assert_eq!(
            Ok(("", Atom::Comment)),
            atom::<NomError>.parse(";d;;dd,ef,,")
        )
    }
}

mod modifier {
    use std::num::{NonZeroU16, NonZeroU8};

    use const_format::concatcp;
    use nom::error::{Error, ErrorKind, VerboseError};

    use super::{NomError, FLATATOM_SAMPLE_STRING as SAMPLE_STR};
    use crate::bng::score::{modifier, Modifier};

    #[test]
    fn volume() {
        assert_eq!(
            Ok((SAMPLE_STR, Modifier::Volume(2))),
            modifier::<NomError>(concatcp!(Modifier::VOLUME, 2u8, SAMPLE_STR))
        );
        assert_eq!(
            Err(nom::Err::Error(Error::new(
                concatcp!(Modifier::VOLUME, 2556u16, SAMPLE_STR),
                ErrorKind::Char
            ))),
            modifier(concatcp!(Modifier::VOLUME, 2556u16, SAMPLE_STR))
        );
    }

    #[test]
    fn octave() {
        assert_eq!(
            Ok((SAMPLE_STR, Modifier::Octave(2))),
            modifier::<NomError>(concatcp!(Modifier::OCTAVE, 2u8, SAMPLE_STR))
        );
        assert_eq!(
            Err(nom::Err::Error(Error::new(
                concatcp!(Modifier::OCTAVE, 2556u16, SAMPLE_STR),
                ErrorKind::Char
            ))),
            modifier(concatcp!(Modifier::OCTAVE, 2556u16, SAMPLE_STR))
        );
    }

    #[test]
    fn length() {
        assert_eq!(
            Ok((
                SAMPLE_STR,
                Modifier::Length(unsafe { NonZeroU8::new_unchecked(2) })
            )),
            modifier::<NomError>(concatcp!(Modifier::LENGTH, 2u8, SAMPLE_STR))
        );
        assert_eq!(
            Err(nom::Err::Error(Error::new(
                concatcp!(Modifier::LENGTH, 2556u16, SAMPLE_STR),
                ErrorKind::Char
            ))),
            modifier(concatcp!(Modifier::LENGTH, 2556u16, SAMPLE_STR))
        );
        assert_eq!(
            Err(nom::Err::Error(Error::new(
                concatcp!(Modifier::LENGTH, 0u8, SAMPLE_STR),
                ErrorKind::Char
            ))),
            modifier(concatcp!(Modifier::LENGTH, 0u8, SAMPLE_STR))
        );
    }

    #[test]
    fn tempo() {
        assert_eq!(
            Ok((
                SAMPLE_STR,
                Modifier::Tempo(unsafe { NonZeroU16::new_unchecked(2) })
            )),
            modifier::<NomError>(concatcp!(Modifier::TEMPO, 2u8, SAMPLE_STR))
        );
        assert_eq!(
            Err(nom::Err::Error(Error::new(
                concatcp!(655353u32, SAMPLE_STR),
                ErrorKind::Digit
            ))),
            modifier(concatcp!(Modifier::TEMPO, 655353u32, SAMPLE_STR))
        );
    }
}

mod quick_modifier {
    use const_format::concatcp;
    use nom::error::VerboseError;

    use super::{NomError, FLATATOM_SAMPLE_STRING as SAMPLE_STR};
    use crate::bng::score::{
        lex::{DOWN, OFF, ON, UP},
        quick_modifier, QuickModifier,
    };

    #[test]
    fn volume() {
        assert_eq!(
            Ok((SAMPLE_STR, QuickModifier::Volume(UP))),
            quick_modifier::<NomError>(concatcp!(QuickModifier::VOLUME.0, SAMPLE_STR))
        );
        assert_eq!(
            Ok((SAMPLE_STR, QuickModifier::Volume(DOWN))),
            quick_modifier::<NomError>(concatcp!(QuickModifier::VOLUME.1, SAMPLE_STR))
        );
    }

    #[test]
    fn octave() {
        assert_eq!(
            Ok((SAMPLE_STR, QuickModifier::Octave(UP))),
            quick_modifier::<NomError>(concatcp!(QuickModifier::OCTAVE.0, SAMPLE_STR))
        );
        assert_eq!(
            Ok((SAMPLE_STR, QuickModifier::Octave(DOWN))),
            quick_modifier::<NomError>(concatcp!(QuickModifier::OCTAVE.1, SAMPLE_STR))
        );
    }

    #[test]
    fn length() {
        assert_eq!(
            Ok((SAMPLE_STR, QuickModifier::Length(UP))),
            quick_modifier::<NomError>(concatcp!(QuickModifier::LENGTH.0, SAMPLE_STR))
        );
        assert_eq!(
            Ok((SAMPLE_STR, QuickModifier::Length(DOWN))),
            quick_modifier::<NomError>(concatcp!(QuickModifier::LENGTH.1, SAMPLE_STR))
        );
    }

    #[test]
    fn pizz() {
        assert_eq!(
            Ok((SAMPLE_STR, QuickModifier::Pizz(ON))),
            quick_modifier::<NomError>(concatcp!(QuickModifier::PIZZ.0, SAMPLE_STR))
        );
        assert_eq!(
            Ok((SAMPLE_STR, QuickModifier::Pizz(OFF))),
            quick_modifier::<NomError>(concatcp!(QuickModifier::PIZZ.1, SAMPLE_STR))
        );
    }
}

#[cfg(test)]
mod slope_modifier {
    use const_format::concatcp;
    use nom::error::VerboseError;

    use super::{NomError, FLATATOM_FASTEVAL_INSTRUCTION as INSTRUCTION};
    use crate::bng::score::{slope_modifier, Atom, SlopeModifier};

    const SAMPLE_STR: &str = concatcp!(' ', INSTRUCTION, Atom::SLOPE.1);

    #[test]
    fn note() {
        assert_eq!(
            Ok((SAMPLE_STR, SlopeModifier::Note)),
            slope_modifier::<NomError>(concatcp!(SlopeModifier::NOTE, SAMPLE_STR))
        )
    }

    #[test]
    fn volume() {
        assert_eq!(
            Ok((SAMPLE_STR, SlopeModifier::Volume)),
            slope_modifier::<NomError>(concatcp!(SlopeModifier::VOLUME, SAMPLE_STR))
        )
    }

    #[test]
    fn octave() {
        assert_eq!(
            Ok((SAMPLE_STR, SlopeModifier::Octave)),
            slope_modifier::<NomError>(concatcp!(SlopeModifier::OCTAVE, SAMPLE_STR))
        )
    }

    #[test]
    fn length() {
        assert_eq!(
            Ok((SAMPLE_STR, SlopeModifier::Length)),
            slope_modifier::<NomError>(concatcp!(SlopeModifier::LENGTH, SAMPLE_STR))
        )
    }

    #[test]
    fn tempo() {
        assert_eq!(
            Ok((SAMPLE_STR, SlopeModifier::Tempo)),
            slope_modifier::<NomError>(concatcp!(SlopeModifier::TEMPO, SAMPLE_STR))
        )
    }
}
