use std::{
    collections::BTreeMap,
    num::{NonZeroU16, NonZeroU8, TryFromIntError},
    sync::{Mutex, MutexGuard, PoisonError},
};

use clap::builder::TypedValueParser;
use const_format::concatcp;
use fasteval::Compiler;
use lazy_static::lazy_static;
use nom::{
    branch::alt,
    bytes::complete::{is_not, tag, take_till, take_till1},
    character::complete::{anychar, char, one_of, space1, u16, u8},
    combinator::{all_consuming, cut, eof, map_opt, map_res, not, opt, value, verify},
    error::{context, ContextError, ErrorKind, FromExternalError, ParseError},
    multi::{many0, many1, many_till},
    sequence::{delimited, pair, preceded, separated_pair, terminated},
    Err, IResult, Parser,
};

use crate::bng::score::Atoms;

use super::{
    super::super::Expression as Instruction, Atom, Modifier, QuickModifier, SlopeModifier,
};

#[cfg(test)]
mod tests;

lazy_static! {
    static ref NOTES: Mutex<Option<String>> = Mutex::new(None);
}

pub fn set_notes(
    notes: impl ToString,
) -> Result<Option<String>, PoisonError<MutexGuard<'static, Option<String>>>> {
    Ok(NOTES.lock()?.replace(notes.to_string()))
}

fn maybe_yml_str_space<'a, E>(i: &'a str) -> IResult<&'a str, Vec<char>, E>
where
    E: nom::error::ParseError<&'a str> + ContextError<&'a str>,
{
    context("yml white space", many0(one_of(" \t\r\n"))).parse(i)
}

pub fn root<'a, E>(i: &'a str) -> IResult<&'a str, Atoms, E>
where
    E: ParseError<&'a str>
        + ContextError<&'a str>
        + FromExternalError<&'a str, TryFromIntError>
        + FromExternalError<&'a str, E>,
{
    many0(delimited(maybe_yml_str_space, atom, maybe_yml_str_space))
        .map(Atoms)
        .parse(i)
}

fn note<'a, E>(i: &'a str) -> IResult<&'a str, Atom, E>
where
    E: ParseError<&'a str> + ContextError<&'a str> + FromExternalError<&'a str, TryFromIntError>,
{
    if let Some(notes) = NOTES.lock().unwrap().as_deref() {
        context(
            "note",
            map_res(map_opt(one_of(notes), |c| notes.find(c)), u8::try_from),
        )
        .map(Atom::Note)
        .parse(i)
    } else {
        panic!("trouble locking the note set!")
    }
}

fn rest<'a, E>(i: &'a str) -> IResult<&'a str, Atom, E>
where
    E: ParseError<&'a str> + ContextError<&'a str>,
{
    value(Atom::Rest, context("rest", char(Atom::REST))).parse(i)
}

fn start_here<'a, E>(i: &'a str) -> IResult<&'a str, Atom, E>
where
    E: ParseError<&'a str> + ContextError<&'a str>,
{
    value(
        Atom::StartHere,
        context("start_here", char(Atom::START_HERE)),
    )
    .parse(i)
}

fn modifier<'a, E>(i: &'a str) -> IResult<&'a str, Atom, E>
where
    E: ParseError<&'a str> + ContextError<&'a str>,
{
    use super::super::modifier;
    context("modifier", preceded(char(Atom::MODIFIER), cut(modifier)))
        .map(Atom::Modifier)
        .parse(i)
}

fn quick_modifier<'a, E>(i: &'a str) -> IResult<&'a str, Atom, E>
where
    E: ParseError<&'a str> + ContextError<&'a str>,
{
    use super::super::quick_modifier;
    context("quick_modifier", quick_modifier)
        .map(Atom::QuickModifier)
        .parse(i)
}

fn r#loop<'a, E>(i: &'a str) -> IResult<&'a str, Atom, E>
where
    E: ParseError<&'a str>
        + ContextError<&'a str>
        + FromExternalError<&'a str, TryFromIntError>
        + FromExternalError<&'a str, E>,
{
    context(
        "loop",
        delimited(
            char(Atom::LOOP.0),
            cut(pair(
                map_opt(opt(u8), |n| {
                    if let Some(n) = n {
                        NonZeroU8::new(n)
                    } else {
                        unsafe { Some(NonZeroU8::new_unchecked(2)) }
                    }
                }),
                root,
            )),
            cut(char(Atom::LOOP.1)),
        ),
    )
    .map(|(n, v)| Atom::Loop(n, v))
    .parse(i)
}

fn tuple<'a, E>(i: &'a str) -> IResult<&'a str, Atom, E>
where
    E: ParseError<&'a str>
        + ContextError<&'a str>
        + FromExternalError<&'a str, TryFromIntError>
        + FromExternalError<&'a str, E>,
{
    context(
        "tuple",
        delimited(char(Atom::TUPLE.0), cut(root), cut(char(Atom::TUPLE.1))),
    )
    .map(Atom::Tuple)
    .parse(i)
}

fn slope<'a, E>(i: &'a str) -> IResult<&'a str, Atom, E>
where
    E: ParseError<&'a str>
        + ContextError<&'a str>
        + FromExternalError<&'a str, TryFromIntError>
        + FromExternalError<&'a str, E>,
{
    use super::super::slope_modifier;
    context(
        "slope_starts",
        delimited(
            char(Atom::SLOPE.0),
            cut(separated_pair(
                separated_pair(
                    slope_modifier,
                    char(' '),
                    map_res(take_till1(|c| c == ','), |s: &str| {
                        s.parse()
                            .map_err(|_| E::from_error_kind(s, ErrorKind::Verify))
                    }),
                ),
                char(','),
                root,
            )),
            cut(char(Atom::SLOPE.1)),
        ),
    )
    .map(|((sm, i), v)| Atom::Slope(sm, i, v))
    .parse(i)
}

fn comment<'a, E>(i: &'a str) -> IResult<&'a str, Atom, E>
where
    E: ParseError<&'a str> + ContextError<&'a str>,
{
    context(
        "comment",
        value(
            Atom::Comment,
            delimited(
                char(Atom::COMMENT.0),
                many0(alt((
                    value((), comment),
                    value((), is_not(concatcp!(Atom::COMMENT.0, Atom::COMMENT.1))),
                ))),
                cut(char(Atom::COMMENT.1)),
            ),
        ),
    )(i)
}

fn atom<'a, E>(i: &'a str) -> IResult<&'a str, Atom, E>
where
    E: ParseError<&'a str>
        + ContextError<&'a str>
        + FromExternalError<&'a str, TryFromIntError>
        + FromExternalError<&'a str, E>,
{
    context(
        "atom",
        alt((
            comment,
            start_here,
            modifier,
            quick_modifier,
            r#loop,
            tuple,
            slope,
            rest,
            note,
        )),
    )
    .parse(i)
}
