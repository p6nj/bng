use std::{
    fmt::{Debug, Display},
    num::TryFromIntError,
    ops::Deref,
};

use derive_new::new;
use naan::fun::{F1Once, F2Once};
use nom::{
    character::complete::one_of,
    combinator::all_consuming,
    error::{context, convert_error, ContextError, FromExternalError, ParseError, VerboseError},
    multi::{many0, many1},
    sequence::{preceded, terminated},
    IResult, Needed, Parser,
};
use serde::{
    de::{self, Deserializer, Visitor},
    Deserialize,
};
use thiserror::Error;

use crate::bng::score::lex::lexer::{root, set_notes};

use super::{Atom, Atoms};

impl<'de> Deserialize<'de> for Atoms {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "lowercase")]
        enum Field {
            Notes,
            Sheet,
        }
        #[derive(Deserialize, new)]
        struct NotesSheet {
            notes: String,
            sheet: String,
        }
        let NotesSheet { notes, sheet } = NotesSheet::deserialize(deserializer)?;
        if sheet.is_empty() {
            Ok(Default::default())
        } else {
            set_notes(notes).unwrap();
            all_consuming(root)
                .parse(&sheet)
                .map_err(|e| pretty_verbose_err.curry().call1(sheet.as_str()).call1(e))
                .map_err(de::Error::custom)
                .map(|(i, r)| r)
        }
    }
}

fn pretty_verbose_err<I>(input: I, e: nom::Err<VerboseError<I>>) -> String
where
    I: Deref<Target = str>,
{
    match e {
        nom::Err::Incomplete(Needed::Unknown) => "needed some more bytes".to_string(),
        nom::Err::Incomplete(Needed::Size(n)) => format!("needed {} more bytes", n),
        nom::Err::Error(e) | nom::Err::Failure(e) => convert_error(input, e),
    }
}
