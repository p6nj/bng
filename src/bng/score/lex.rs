use super::{Atom, Modifier, QuickModifier, SlopeModifier};

pub(super) mod lexer;

pub(super) const ON: bool = true;
pub(super) const OFF: bool = false;
pub(super) const UP: bool = true;
pub(super) const DOWN: bool = false;

struct WrappingTokens;
impl WrappingTokens {
    const PARENTHESES: (char, char) = ('(', ')');
    const SQUARE_BRACKETS: (char, char) = ('[', ']');
    const BRACKETS: (char, char) = ('{', '}');
    const SEMICOLON_COMMA: (char, char) = (';', ',');
    const PLUS_MINUS: (char, char) = ('+', '-');
    const RIGHT_LEFT: (char, char) = ('>', '<');
    const SLASH_BACKSLASH: (char, char) = ('/', '\\');
    const QUOTE_DEG: (char, char) = ('\'', '°');
}

impl QuickModifier {
    pub(super) const VOLUME: (char, char) = WrappingTokens::PLUS_MINUS;
    pub(super) const OCTAVE: (char, char) = WrappingTokens::RIGHT_LEFT;
    pub(super) const LENGTH: (char, char) = WrappingTokens::SLASH_BACKSLASH;
    pub(super) const PIZZ: (char, char) = WrappingTokens::QUOTE_DEG;
}

impl Modifier {
    pub(super) const VOLUME: char = 'v';
    pub(super) const OCTAVE: char = 'o';
    pub(super) const LENGTH: char = 'l';
    pub(super) const TEMPO: char = 't';
}

impl SlopeModifier {
    pub(super) const NOTE: char = 'n';
    pub(super) const VOLUME: char = 'v';
    pub(super) const OCTAVE: char = 'o';
    pub(super) const LENGTH: char = 'l';
    pub(super) const TEMPO: char = 't';
}

impl Atom {
    pub(super) const REST: char = '.';
    pub(super) const START_HERE: char = ':';
    pub(super) const MODIFIER: char = '!';
    pub(super) const LOOP: (char, char) = WrappingTokens::PARENTHESES;
    pub(super) const TUPLE: (char, char) = WrappingTokens::SQUARE_BRACKETS;
    pub(super) const SLOPE: (char, char) = WrappingTokens::BRACKETS;
    pub(super) const COMMENT: (char, char) = WrappingTokens::SEMICOLON_COMMA;
}
