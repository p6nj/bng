use std::{fmt::Display, marker::PhantomData, str::FromStr};

use fasteval::Compiler;
use serde::{de::Visitor, Deserialize};

use super::{Expression, KeyboardMapping, Scale};

mod from_str;

struct FromStrVisitor<'a, V> {
    expecting: &'a str,
    phantom: PhantomData<V>,
}

impl<'a, V> FromStrVisitor<'a, V> {
    fn new(expecting: &'a str) -> Self {
        FromStrVisitor {
            expecting,
            phantom: PhantomData,
        }
    }
}

impl<'de, 'a, V> Visitor<'de> for FromStrVisitor<'a, V>
where
    V: FromStr,
    <V as FromStr>::Err: Display,
{
    type Value = V;
    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str(self.expecting)
    }
    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        v.parse::<V>().map_err(serde::de::Error::custom)
    }
}

macro_rules! deserialize_fromstr_expect {
    ($type:ty, $expect:literal) => {
        impl<'de> serde::de::Deserialize<'de> for $type {
            fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
            where
                D: serde::Deserializer<'de>,
            {
                deserializer.deserialize_str(FromStrVisitor::new($expect))
            }
        }
    };
}

deserialize_fromstr_expect!(
    Expression,
    "a math expression following fasteval syntax (https://docs.rs/fasteval)"
);

deserialize_fromstr_expect!(Scale, "a .scl file containing a Scala scale");
deserialize_fromstr_expect!(
    KeyboardMapping,
    "a .kbm file containing a Scala keyboard mapping"
);
