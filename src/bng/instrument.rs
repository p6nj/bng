use std::collections::HashMap;

#[cfg(debug_assertions)]
use super::Expression as Instruction;
use derive_new::new;
use derived_deref::Deref;
use serde::Deserialize;

#[derive(Deref, new, Deserialize)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Instrument {
    #[target]
    expr: Instruction,
    vars: Option<HashMap<String, f64>>,
}
