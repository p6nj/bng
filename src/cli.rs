use std::{convert::Infallible, fmt::Display, fs::read_to_string, io, str::FromStr};

use amplify::{From, Wrapper};
use clap::Parser;
use strum::EnumString;

pub mod doc;

/// Cli entry point
#[derive(Clone, Parser)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub enum BngCli {
    /// Play the song through default sink
    Play(PlayOpts),
    /// Export the song to a sound file
    Export(ExportOpts),
    /// List supported sound file extensions and instrument / song available expressions
    #[command(subcommand)]
    List(ListOpts),
}

/// [`BngCli`] "play" command options
#[derive(Clone, Parser)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct PlayOpts {
    #[arg(value_parser = FileContents::from_str)]
    pub(super) input: FileContents,
}

/// [`BngCli`] "export" command options
#[derive(Clone, Parser)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct ExportOpts {
    /// Input file (written song file)
    #[arg(value_parser = FileContents::from_str)]
    input: FileContents,
    /// Output file (sound file)
    #[arg(value_parser = AudioFileName::from_str)]
    output: AudioFileName,
}

/// [`BngCli`] "list" command sub-commands
#[derive(Clone, Parser)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub enum ListOpts {
    /// List supported sound file extensions to export songs
    Extensions,
    /// Show the math syntax used for instrument definition and slopes
    Math {
        /// Kind of syntax you want to list (functions, operators or literals)
        #[arg(value_parser = MathDocKind::from_str)]
        kind: Option<MathDocKind>,
    },
    /// List available score glyphs and their meaning
    Glyphs,
}

#[derive(Clone, Copy, EnumString)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub enum MathDocKind {
    #[strum(ascii_case_insensitive)]
    Functions,
    #[strum(ascii_case_insensitive)]
    Operators,
    #[strum(ascii_case_insensitive)]
    Literals,
}

#[derive(Clone, Wrapper, From)]
#[wrapper(Deref)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct FileContents(String);

impl FromStr for FileContents {
    type Err = io::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        read_to_string(s).map(Into::into)
    }
}

#[derive(Clone, Wrapper, From)]
#[wrapper(Deref)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct AudioFileName(String);

#[derive(Debug)]
pub struct UnsupportedFileExtensionError;

impl std::error::Error for UnsupportedFileExtensionError {}

impl Display for UnsupportedFileExtensionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "The extension of the selected output sound file is not supported."
        )
    }
}

impl FromStr for AudioFileName {
    type Err = UnsupportedFileExtensionError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(s.to_owned().into())
    }
}
