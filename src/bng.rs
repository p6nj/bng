use std::{collections::HashMap, str::FromStr};

use amplify::{From, Wrapper};
use derive_new::new;
use derived_deref::Deref;
use fasteval::{Compiler, Instruction};
pub(super) use instrument::Instrument;
pub(super) use score::Atom;
use score::Atoms;
use serde::{de::Visitor, Deserialize};
use tune::scala::{Kbm, Scl};

mod de;
mod instrument;
mod score;

#[derive(Debug, PartialEq, Wrapper, From)]
pub struct Expression(Instruction);

#[derive(new, Deserialize)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub(super) struct Channel {
    instr: String,
    score: Atoms,
}

#[derive(Deserialize, Default)]
#[cfg_attr(debug_assertions, derive(new, Debug))]
#[serde(deny_unknown_fields)]
#[serde(default)]
pub(super) struct BngFile {
    instruments: HashMap<String, Instrument>,
    channels: HashMap<String, Channel>,
    #[serde(rename = "scl")]
    scales: HashMap<String, Scale>,
    #[serde(rename = "kbm")]
    keyboard_mappings: HashMap<String, KeyboardMapping>,
}

#[derive(Debug, Wrapper, From)]
struct Scale(Scl);
#[derive(Debug, Wrapper, From)]
struct KeyboardMapping(Kbm);
