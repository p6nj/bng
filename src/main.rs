use std::collections::HashMap;

use anyhow::Error;
use bng::{BngFile, Channel, Expression, Instrument};
/// TODO: remove clap, use only a file or standard in
use clap::Parser;
use cli::{doc, BngCli as Cli, ListOpts, MathDocKind, PlayOpts};
use fasteval::Compiler;

mod bng;
mod cli;

fn main() -> Result<(), Error> {
    // println!("{}", option_env!("TEST").unwrap_or("ok"));
    let args = Cli::parse();
    // #[cfg(debug_assertions)]
    // {
    //     println!("{}", serde_yml::to_string(&bngfile_generator())?);
    //     println!("{:?}", args);
    // }
    match args {
        Cli::Play(PlayOpts { input }) => {
            let bng_file: BngFile = serde_yml::from_str(&input)?;
            #[cfg(debug_assertions)]
            println!("{:#?}", bng_file);
        }
        Cli::List(l) => match l {
            ListOpts::Extensions => println!("you can't export songs yet"),
            ListOpts::Math { kind } => match kind {
                None => println!(
                    "Functions :\n{}\n\nOperators :\n{}\n\nLiterals :\n{}",
                    doc::math::FUNCTIONS,
                    doc::math::OPERATORS,
                    doc::math::LITERALS
                ),
                Some(m) => println!(
                    "{}",
                    match m {
                        MathDocKind::Functions => doc::math::FUNCTIONS,
                        MathDocKind::Operators => doc::math::OPERATORS,
                        MathDocKind::Literals => doc::math::LITERALS,
                    }
                ),
            },
            ListOpts::Glyphs => println!("{}", doc::glyphs::ALL),
        },
        Cli::Export(_) => unimplemented!("can't do that yet"),
    }
    Ok(())
}
