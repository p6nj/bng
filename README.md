# bng
[![Crates.io](https://img.shields.io/crates/v/bng.svg)](https://crates.io/crates/bng)
[![Docs.rs](https://docs.rs/bng/badge.svg)](https://docs.rs/bng)
[![License: MIT](https://img.shields.io/badge/License-NWSL-yellow.svg)](https://gist.github.com/DavidBuchanan314/35cb9f8a2f754b9a03a74bed19575661)

Bleeperpreter New Gen - the smarter 'preter!
A better [Bleeperpreter](https://github.com/p6nj/bleeperpreter) from scratch.
