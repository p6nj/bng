extern crate proc_macro;
use inflector::Inflector;
use proc_macro::TokenStream;
use proc_macro2::Span;
use quote::{quote, ToTokens};
use syn::{parse_macro_input, Data, DataEnum, DeriveInput, Fields, FieldsUnnamed, Ident};

#[proc_macro_derive(SlopeModifierParser)]
pub fn slope_modifier_parser(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    impl_slope_modifier_parser(ast)
}

#[proc_macro_derive(QuickModifierParser)]
pub fn quick_modifier_parser(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    impl_quick_modifier_parser(ast)
}

#[proc_macro_derive(ModifierParser)]
pub fn modifier_parser(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    impl_modifier_parser(ast)
}

trait ToIdent: ToString {
    fn to_ident(&self) -> Ident {
        Ident::new(&self.to_string(), Span::call_site())
    }
}

impl<S: ToString> ToIdent for S {}

fn impl_slope_modifier_parser(ast: DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let fn_name = name.to_string().to_snake_case().to_ident();
    if let Data::Enum(DataEnum { variants, .. }) = ast.data {
        let match_arms = variants.iter().map(|variant| {
            let variant_name = &variant.ident;
            let variant_name_lower = variant_name.to_string().to_lowercase().to_ident();
            let const_name = &variant.ident.to_string().to_uppercase().to_ident();
            quote! {
                nom::error::context(
                    stringify!(#variant_name_lower),
                    nom::combinator::value(
                        #name::#variant_name,
                        nom::character::complete::char(SlopeModifier::#const_name)
                    )
                )
            }
        });

        quote! {
            pub fn #fn_name<'a, E: nom::error::ParseError<&'a str> + nom::error::ContextError<&'a str>>(
                i: &'a str,
            ) -> nom::IResult<&'a str, #name, E> {
                nom::error::context(
                    "slope modifier",
                    nom::branch::alt((
                        #(#match_arms),*
                    ))
                )(i)
            }
        }
        .into()
    } else {
        panic!("this macro only works on enums")
    }
}

fn impl_quick_modifier_parser(ast: DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let fn_name = name.to_string().to_snake_case().to_ident();
    if let Data::Enum(DataEnum { variants, .. }) = ast.data {
        let match_arms = variants.iter().map(|variant| {
            let variant_name = &variant.ident;
            let variant_name_lower = variant_name.to_string().to_lowercase().to_ident();
            let const_name = &variant.ident.to_string().to_uppercase().to_ident();
            quote! {
                nom::error::context(
                    stringify!(#variant_name_lower),
                    nom::combinator::map(
                        nom::branch::alt(
                            (
                                nom::combinator::value(
                                    lex::UP,
                                    nom::character::complete::char(#name::#const_name.0)
                                ),
                                nom::combinator::value(
                                    lex::DOWN,
                                    nom::character::complete::char(#name::#const_name.1)
                                )
                            )
                        ),
                        #name::#variant_name
                    )
                )
            }
        });

        quote! {
            pub fn #fn_name<'a, E: nom::error::ParseError<&'a str> + nom::error::ContextError<&'a str>>(
                i: &'a str,
            ) -> nom::IResult<&'a str, #name, E> {
                nom::error::context(
                    "quick modifier",
                    nom::branch::alt((
                        #(#match_arms),*
                    ))
                )(i)
            }
        }
        .into()
    } else {
        panic!("this macro only works on enums")
    }
}

fn impl_modifier_parser(ast: DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let fn_name = name.to_string().to_snake_case().to_ident();
    if let Data::Enum(DataEnum { variants, .. }) = ast.data {
        let match_arms = variants.iter().map(|variant| {
            let variant_name = &variant.ident;
            let variant_name_lower = variant_name.to_string().to_lowercase().to_ident();
            let const_name = &variant.ident.to_string().to_uppercase().to_ident();
            if let Fields::Unnamed(FieldsUnnamed {
                paren_token: _,
                unnamed,
            }) = &variant.fields
            {
                let type_name = {
                    let type_name = &unnamed[0].ty.to_token_stream().to_string();
                    match type_name.strip_prefix("NonZero") {
                        Some(s) => {
                            let lower = s.to_lowercase().to_ident();
                            let type_name = &unnamed[0].ty;
                            quote! {
                                nom::combinator::map_opt(
                                    nom::character::complete::#lower,
                                    std::num::#type_name::new
                                )
                            }
                        }
                        _ => {
                            let type_name = &unnamed[0].ty;
                            quote! {
                                nom::character::complete::#type_name
                            }
                        }
                    }
                };
                quote! {
                    nom::error::context(
                        stringify!(#variant_name_lower),
                        nom::combinator::map(
                            nom::sequence::preceded(nom::character::complete::char(#name::#const_name), #type_name),
                            #name::#variant_name
                        )
                    )
                }
            } else {
                panic!("this macro only works on unnamed fields")
            }
        });

        quote! {
            pub fn #fn_name<'a, E: nom::error::ParseError<&'a str> + nom::error::ContextError<&'a str>>(
                i: &'a str,
            ) -> nom::IResult<&'a str, #name, E> {
                nom::error::context(
                    "modifier",
                    nom::branch::alt((
                        #(#match_arms),*
                    ))
                )(i)
            }
        }
        .into()
    } else {
        panic!("this macro only works on enums")
    }
}
